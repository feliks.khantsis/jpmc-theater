package com.jpmc.theater;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class ShowingTests {

	private static final double MOVIE_PRICE = 12.0;
	
	private Movie movie = new Movie("MovieTitle", Duration.of(100, ChronoUnit.MINUTES), MOVIE_PRICE, 0);
	private Movie specialMovie = new Movie("SpMovieTitle", Duration.of(90, ChronoUnit.MINUTES), 11, 1);

	Showing showing, showing2, showing3, showing4;

//	@BeforeEach
//	void setup() {
//		showing2 = new Showing(movie, 2, LocalDateTime.of(2020, 2, 1, 14, 0));
//		showing4 = new Showing(specialMovie, 1, LocalDateTime.of(2020, 2, 1, 12, 0));
//	}

	@Test
	void testNoDiscount() {
		showing = new Showing(movie, 4, LocalDateTime.of(2020, 2, 1, 10, 30));
		showing2 = new Showing(movie, 4, LocalDateTime.of(2020, 2, 1, 19, 0));
		assertEquals(MOVIE_PRICE, showing.getPrice());
		assertEquals(MOVIE_PRICE, showing2.getPrice());
	}
	
	@Test
	void testSpecialMovieDiscount() {
		showing = new Showing(specialMovie, 4, LocalDateTime.of(2020, 2, 1, 10, 30));
		assertEquals(11.0 * 0.8, showing.getPrice());
	}
	
	@Test
	void test1stShowDiscount() {
		showing = new Showing(movie, 1, LocalDateTime.of(2020, 2, 1, 10, 30));
		assertEquals(MOVIE_PRICE-3.0, showing.getPrice());
	}
	
	@Test
	void test2stShowDiscount() {
		showing = new Showing(movie, 2, LocalDateTime.of(2020, 2, 1, 10, 30));
		assertEquals(MOVIE_PRICE-2.0, showing.getPrice());
	}
	
	@Test
	void testTimeDiscount() {
		showing = new Showing(movie, 4, LocalDateTime.of(2020, 2, 1, 11, 30));
		assertEquals(MOVIE_PRICE*0.75, showing.getPrice());
	}
	
	@Test
	void testDateDiscount() {
		showing = new Showing(movie, 4, LocalDateTime.of(2020, 2, 7, 10, 30));
		assertEquals(MOVIE_PRICE-1, showing.getPrice());
	}
	
	@Test
	void testOnlyLargestDiscount() {
		showing = new Showing(movie, 2, LocalDateTime.of(2020, 2, 7, 10, 30));
		assertEquals(MOVIE_PRICE-2, showing.getPrice(), "2nd showing should override 7th of the month");
		showing = new Showing(specialMovie, 4, LocalDateTime.of(2020, 2, 7, 10, 30));
		assertEquals(11*0.8, showing.getPrice(), "special score should override 7th of the month");
		showing = new Showing(movie, 1, LocalDateTime.of(2020, 2, 4, 11, 30));
		assertEquals(MOVIE_PRICE*0.75, showing.getPrice(), "only one of showing and time discounts should be applied");
		showing = new Showing(specialMovie, 1, LocalDateTime.of(2020, 2, 7, 12, 0));
		assertEquals(11-3, showing.getPrice(), "only one of showing and time discounts should be applied");
	}
	
	@Nested
	class ReservationTests {
		
		@BeforeEach
		void setup() {
			showing = new Showing(movie, 4, LocalDateTime.of(2020, 2, 3, 10, 30));
		}
		
		@Test
		void testSimpleResevation() {
			showing.setAudienceSize(23);
			assertEquals(23*MOVIE_PRICE, showing.getEarnings());
		}
		
		@Test
		void testDoubleResevation() {
			showing.setAudienceSize(23);
			showing.setAudienceSize(11);
			assertEquals(11*MOVIE_PRICE, showing.getEarnings());
		}
		
		@Test
		void testWithDiscounts() {
			showing = new Showing(movie, 4, LocalDateTime.of(2020, 2, 1, 11, 30));
			showing.setAudienceSize(10);
			assertEquals(MOVIE_PRICE*0.75*10, showing.getEarnings());
		}
	}
}
