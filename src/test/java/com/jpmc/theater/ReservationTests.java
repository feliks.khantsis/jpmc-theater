package com.jpmc.theater;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.function.Consumer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

@SuppressWarnings("unchecked")
public class ReservationTests {

	private Customer customer = new Customer("John Doe", "unused-id");;
	private Showing showing;
	
	@BeforeEach
	void setup() {
		showing = mock(Showing.class);
        when(showing.getPrice()).thenReturn(15.0);
	}
	
    @Test
    void totalFeeIsPriceTimesAudience() {
    	assertAll("total fee should be showing price times reservation size",
    			() -> assertEquals(new Reservation(customer, showing, 3, null).getTotalFee(), 45.0),
    			() -> assertEquals(new Reservation(customer, showing, 4, null).getTotalFee(), 60.0),
    			() -> assertEquals(new Reservation(customer, showing, 0, null).getTotalFee(), 0));
    }
    
    @Test
    void cancelCalled() {
    	Consumer<Reservation> mock = mock(Consumer.class);
    	Reservation reserve = new Reservation(customer, showing, 3, mock);
    	reserve.cancel();
    	verify(mock).accept(reserve);
    }
    

    @Test
    void cancelCalledOnce() {
    	Consumer<Reservation> mock = mock(Consumer.class);
    	Reservation reserve = new Reservation(customer, showing, 3, mock);
    	reserve.cancel();
    	reserve.cancel();
    	verify(mock, times(1)).accept(reserve);
    }
    
	@Test
    void cancelCalledOnlyWhenCancelled() {
    	Consumer<Reservation> mock = mock(Consumer.class);
    	Reservation reserve = new Reservation(customer, showing, 3, mock);
    	verify(mock, never()).accept(reserve);
    }
}
