package com.jpmc.theater;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TheaterTests {

	static class TestProvider extends LocalDateProvider {
		LocalDate date;

		TestProvider(LocalDate date) {
			this.date = date;
		}

		public LocalDate currentDate() {
			return date;

		}
	}

	LocalDateProvider prov;
	Customer john;
	Theater theater;
	Showing showing;
	List<Showing> schedule;

	Movie spiderMan, turningRed, theBatMan;

	@BeforeEach
	void setup() {
		LocalDate testDate = LocalDate.of(2000, 5, 6);
		prov = new TestProvider(testDate);
		theater = new Theater(prov);
		Movie movie = new Movie("MovieTitle", Duration.of(120, ChronoUnit.MINUTES), 15, 0);
		showing = new Showing(movie, 4, LocalDateTime.of(2000, 6, 5, 11, 0));

		john = new Customer("John Doe", "id-12345");

		spiderMan = new Movie("Spider-Man: No Way Home", Duration.ofMinutes(90), 12.5, Movie.MOVIE_CODE_SPECIAL);
		turningRed = new Movie("Turning Red", Duration.ofMinutes(85), 11, 0);
		theBatMan = new Movie("The Batman", Duration.ofMinutes(95), 9, 0);

		schedule = List.of(new Showing(turningRed, 1, LocalDateTime.of(2020, 2, 3, 10, 0)),
				new Showing(spiderMan, 2, LocalDateTime.of(2020, 2, 3, 10, 0)),
				new Showing(theBatMan, 3, LocalDateTime.of(2020, 2, 3, 10, 0)),
				new Showing(turningRed, 4, LocalDateTime.of(2020, 2, 3, 10, 0)),
				new Showing(spiderMan, 5, LocalDateTime.of(2020, 2, 3, 10, 0)),
				new Showing(theBatMan, 6, LocalDateTime.of(2020, 2, 3, 10, 0)),
				new Showing(turningRed, 7, LocalDateTime.of(2020, 2, 3, 10, 0)),
				new Showing(spiderMan, 8, LocalDateTime.of(2020, 2, 3, 10, 0)),
				new Showing(theBatMan, 9, LocalDateTime.of(2020, 2, 3, 10, 0)));
		theater.setSchedule(schedule);
	}

	@Test
	void setScheduleMatches() {
		theater.setSchedule(schedule);
		assertEquals(schedule, theater.getSchedule());

		schedule = List.of(new Showing(turningRed, 1, LocalDateTime.of(2020, 2, 3, 10, 0)),
				new Showing(turningRed, 2, LocalDateTime.of(2020, 2, 3, 10, 0)));
		theater.setSchedule(schedule);
		assertEquals(schedule, theater.getSchedule());
	}

	@Test
	void totalReservation() {
		Reservation reservation = theater.reserve(john, showing, 4);
		Reservation reservation2 = theater.reserve(john, showing, 3);
		int totalAudience = reservation.getAudienceCount() + reservation2.getAudienceCount();
		assertEquals(totalAudience, showing.getAudienceSize());
		assertEquals(totalAudience * showing.getPrice(), showing.getEarnings());
	}
	
	@Test
	void cancelReservations() {
		Reservation reservation = theater.reserve(john, showing, 4);
		Reservation reservation2 = theater.reserve(john, showing, 3);
		reservation2.cancel();
		assertEquals(4, showing.getAudienceSize());
		assertEquals(4 * showing.getPrice(), showing.getEarnings());
		reservation.cancel();
		assertEquals(0, showing.getAudienceSize());
		assertEquals(0, showing.getEarnings());
	}

	@Test
	void printMovieSchedule() throws UnsupportedEncodingException {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		theater.printSchedule(new PrintStream(os));
		String printedString = os.toString("UTF-8");
		assertEquals(("2000-05-06\n" + "===================================================\n"
				+ "1: 2020-02-03T10:00 Turning Red (1 hour 25 minutes) $0.0\n"
				+ "2: 2020-02-03T10:00 Spider-Man: No Way Home (1 hour 30 minutes) $0.0\n"
				+ "3: 2020-02-03T10:00 The Batman (1 hour 35 minutes) $0.0\n"
				+ "4: 2020-02-03T10:00 Turning Red (1 hour 25 minutes) $0.0\n"
				+ "5: 2020-02-03T10:00 Spider-Man: No Way Home (1 hour 30 minutes) $0.0\n"
				+ "6: 2020-02-03T10:00 The Batman (1 hour 35 minutes) $0.0\n"
				+ "7: 2020-02-03T10:00 Turning Red (1 hour 25 minutes) $0.0\n"
				+ "8: 2020-02-03T10:00 Spider-Man: No Way Home (1 hour 30 minutes) $0.0\n"
				+ "9: 2020-02-03T10:00 The Batman (1 hour 35 minutes) $0.0\n"
				+ "===================================================\n")
				.replace("\n", System.getProperty("line.separator")), printedString, "wrong printed string");
	}
	
}
