package com.jpmc.theater;

import java.io.IOException;
import java.io.PrintStream;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.exc.StreamWriteException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public class Theater {

	@JsonIgnore
	private LocalDateProvider provider;

	private List<Showing> schedule;

	public Theater(LocalDateProvider provider) {
		this.provider = provider;
		this.schedule = Collections.emptyList();
	}

	public void setSchedule(List<Showing> schedule) {
		this.schedule = List.copyOf(schedule);
	}

	public List<Showing> getSchedule() {
		return schedule;
	}

	/**
	 * Reserves a number of seats for the customer
	 * @param customer
	 * @param showing
	 * @param howManyTickets
	 * @return the reservation object
	 */
	public Reservation reserve(Customer customer, Showing showing, int howManyTickets) {
		showing.setAudienceSize(showing.getAudienceSize() + howManyTickets);
		return new Reservation(customer, showing, howManyTickets, 
				res -> showing.setAudienceSize(showing.getAudienceSize()-res.getAudienceCount()));
	}

	public void printSchedule(PrintStream out) {
		out.println(provider.currentDate());
		out.println("===================================================");
		schedule.forEach(
				s -> out.println(s.getSequenceOfTheDay() + ": " + s.getStartTime() + " " + s.getMovie().getTitle() + " "
						+ humanReadableFormat(s.getMovie().getRunningTime()) + " $" + s.getEarnings()));
		out.println("===================================================");
	}

	private static String humanReadableFormat(Duration duration) {
		long hour = duration.toHours();
		long remainingMin = duration.toMinutes() - TimeUnit.HOURS.toMinutes(duration.toHours());

		return String.format("(%s hour%s %s minute%s)", hour, handlePlural(hour), remainingMin,
				handlePlural(remainingMin));
	}

	// (s) postfix should be added to handle plural correctly
	private static String handlePlural(long value) {
		if (value == 1) {
			return "";
		} else {
			return "s";
		}
	}

	public static void main(String[] args) throws StreamWriteException, DatabindException, IOException {
		LocalDateProvider provider = LocalDateProvider.singleton();
		Theater theater = new Theater(LocalDateProvider.singleton());

		Movie spiderMan = new Movie("Spider-Man: No Way Home", Duration.ofMinutes(90), 12.5, Movie.MOVIE_CODE_SPECIAL);
		Movie turningRed = new Movie("Turning Red", Duration.ofMinutes(85), 11, 0);
		Movie theBatMan = new Movie("The Batman", Duration.ofMinutes(95), 9, 0);
		theater.setSchedule(
				List.of(new Showing(turningRed, 1, LocalDateTime.of(provider.currentDate(), LocalTime.of(9, 0))),
						new Showing(spiderMan, 2, LocalDateTime.of(provider.currentDate(), LocalTime.of(11, 0))),
						new Showing(theBatMan, 3, LocalDateTime.of(provider.currentDate(), LocalTime.of(12, 50))),
						new Showing(turningRed, 4, LocalDateTime.of(provider.currentDate(), LocalTime.of(14, 30))),
						new Showing(spiderMan, 5, LocalDateTime.of(provider.currentDate(), LocalTime.of(16, 10))),
						new Showing(theBatMan, 6, LocalDateTime.of(provider.currentDate(), LocalTime.of(17, 50))),
						new Showing(turningRed, 7, LocalDateTime.of(provider.currentDate(), LocalTime.of(19, 30))),
						new Showing(spiderMan, 8, LocalDateTime.of(provider.currentDate(), LocalTime.of(21, 10))),
						new Showing(theBatMan, 9, LocalDateTime.of(provider.currentDate(), LocalTime.of(23, 0)))));

		theater.printSchedule(System.out);
		
		ObjectMapper mapper = new ObjectMapper()
				.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
				.registerModule(new JavaTimeModule());
		mapper.writeValue(System.out, theater);
	}
}
