package com.jpmc.theater;

import java.time.LocalDate;

public class LocalDateProvider {
	private final static LocalDateProvider instance = new LocalDateProvider();

	/**
	 * @return make sure to return singleton instance
	 */
	public static LocalDateProvider singleton() {
		return instance;
	}

	public LocalDate currentDate() {
		return LocalDate.now();
	}
}
