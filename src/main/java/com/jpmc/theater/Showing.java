package com.jpmc.theater;

import java.time.LocalDateTime;
import java.time.LocalTime;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class Showing {
	
	@JsonUnwrapped
	private Movie movie;
	
	@JsonIgnore
	private int sequenceOfTheDay;
	private LocalDateTime showStartTime;
	private int audienceSize;

	public Showing(Movie movie, int sequenceOfTheDay, LocalDateTime showStartTime) {
		this.movie = movie;
		this.sequenceOfTheDay = sequenceOfTheDay;
		this.showStartTime = showStartTime;
	}

	public Movie getMovie() {
		return movie;
	}

	public LocalDateTime getStartTime() {
		return showStartTime;
	}

	public boolean isSequence(int sequence) {
		return this.sequenceOfTheDay == sequence;
	}

	public int getSequenceOfTheDay() {
		return sequenceOfTheDay;
	}

	public int getAudienceSize() {
		return audienceSize;
	}

	void setAudienceSize(int audienceSize) {
		this.audienceSize = audienceSize;
	}

	@JsonSerialize
	public double getEarnings() {
		return this.getPrice() * audienceSize;
	}

	public double getPrice() {
		return movie.getTicketPrice() - getDiscount();
	}

	private double getDiscount() {
		double specialDiscount = getSpecialDiscount();
		double sequenceDiscount = getSequenceDiscount();
		double timeDiscount = getTimeDiscount();
		double dateDiscount = getDateDiscount();

		// biggest discount wins
		return Math.max(specialDiscount, 
				Math.max(sequenceDiscount, 
						Math.max(timeDiscount, dateDiscount)));
	}

	private double getSpecialDiscount() {
		double specialDiscount = 0;
		if (movie.getSpecialCode() == Movie.MOVIE_CODE_SPECIAL) {
			specialDiscount = movie.getTicketPrice() * 0.2; // 20% discount for special movie
		}
		return specialDiscount;
	}

	private double getSequenceDiscount() {
		double sequenceDiscount = 0;
		if (this.sequenceOfTheDay == 1) {
			sequenceDiscount = 3; // $3 discount for 1st show
		} else if (this.sequenceOfTheDay == 2) {
			sequenceDiscount = 2; // $2 discount for 2nd show
		}
		return sequenceDiscount;
	}

	private double getDateDiscount() {
		return this.showStartTime.getDayOfMonth() == 7 ? 1 : 0;
	}

	private double getTimeDiscount() {
		LocalTime time = showStartTime.toLocalTime();
		if (time.isAfter(LocalTime.of(11, 0)) && time.isBefore(LocalTime.of(16, 0))) {
			return movie.getTicketPrice() * 0.25;
		} else {
			return 0;
		}
	}
}
