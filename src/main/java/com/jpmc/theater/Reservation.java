package com.jpmc.theater;

import java.util.function.Consumer;

public class Reservation {
	private final Customer customer;
	private final Showing showing;
	private final int audienceCount;
	private final Consumer<Reservation> cancellation;
	
	private boolean cancelled;

	/**
	 * Package private constructor to prevent non library construction
	 */
	Reservation(Customer customer, Showing showing, int audienceCount, Consumer<Reservation> cancellation) {
		this.customer = customer;
		this.showing = showing;
		this.audienceCount = audienceCount;
		this.cancellation = cancellation;
	}

	public Customer getCustomer() {
		return customer;
	}

	public Showing getShowing() {
		return showing;
	}

	public int getAudienceCount() {
		return audienceCount;
	}

	public double getTotalFee() {
		return showing.getPrice() * audienceCount;
	}
	
	public void cancel() {
		if(!cancelled) {
			this.cancelled = true;
			if(cancellation != null)
				cancellation.accept(this);
		}
	}
}